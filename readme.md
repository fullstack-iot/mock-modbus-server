# Mock Modbus Server 🦀

![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-modbus-server?branch=01-modbus-record-discrete-input&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/badge/rust-1.79.0-gray?style=for-the-badge&logo=rust)

## Pre-Requisites

 ```shell
cargo install cargo-cmd \
               cargo-audit \
               cargo-udeps --locked \
               cargo2junit
```
