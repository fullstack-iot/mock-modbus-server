FROM rust:1.79.0-slim-bookworm
WORKDIR /app
COPY . .
RUN cargo install --path .
CMD mock-modbus-server