mod simulate;
mod utils;

use crate::simulate::Simulator;
use crate::utils::Config;
use rodbus::server::{
    spawn_tcp_server_task, AddressFilter, IllegalAddressConversion, RequestHandler, ServerHandle,
    ServerHandlerMap, ServerHandlerType,
};
use rodbus::{DecodeLevel, ExceptionCode, UnitId};
use std::error::Error;
use std::net::{IpAddr, SocketAddr};
use utils::{load_config, Level};

const NUM_INPUTS: usize = 1;

struct SimpleHandler {
    discrete_inputs: Vec<bool>,
}
impl SimpleHandler {
    fn new(discrete_inputs: Vec<bool>) -> Self {
        Self { discrete_inputs }
    }
    fn discrete_inputs_as_mut(&mut self) -> &mut Vec<bool> {
        &mut self.discrete_inputs
    }
}

impl RequestHandler for SimpleHandler {
    fn read_discrete_input(&self, address: u16) -> Result<bool, ExceptionCode> {
        self.discrete_inputs.get(address as usize).to_result()
    }
}

fn create_handler(
    config: &Config,
) -> (
    ServerHandlerType<SimpleHandler>,
    ServerHandlerMap<SimpleHandler>,
) {
    // wraps a server handler for passing to the server (so can be shared across threads)
    let handler = SimpleHandler::new(vec![false; NUM_INPUTS]).wrap();
    // map unit ids to a handler for processing requests
    let map = ServerHandlerMap::single(UnitId::new(config.unit_id), handler.clone());
    (handler, map)
}

async fn run_server(
    handler: ServerHandlerType<SimpleHandler>,
    mut server: ServerHandle,
    config: Config,
) -> Result<(), Box<dyn Error>> {
    let simulate = Simulator::new(config.simulation_mode);
    loop {
        {
            let mut handler = handler.lock().unwrap();
            simulate.booleans(handler.discrete_inputs_as_mut());
        }
        tokio::time::sleep(std::time::Duration::from_secs(config.system_rate)).await;
        if false {
            server.set_decode_level(DecodeLevel::default()).await?;
        }
    }
}

pub async fn run_tcp(config: Config) -> Result<(), Box<dyn Error>> {
    let (handler, map) = create_handler(&config);
    let server = spawn_tcp_server_task(
        1,
        SocketAddr::new(IpAddr::V4(config.host), config.port),
        map,
        AddressFilter::Any,
        DecodeLevel::default(),
    )
    .await?;
    run_server(handler, server, config).await
}

#[tokio::main(flavor = "multi_thread")]
async fn main() -> Result<(), Box<dyn Error>> {
    let config = load_config().unwrap();
    tracing_subscriber::fmt()
        .with_max_level(match config.log_level {
            Level::ERROR => tracing::Level::ERROR,
            Level::WARN => tracing::Level::WARN,
            Level::INFO => tracing::Level::INFO,
            Level::DEBUG => tracing::Level::DEBUG,
        })
        .with_target(false)
        .init();
    tracing::info!("Running with: {:?}", config);
    run_tcp(config).await
}

#[cfg(test)]
mod simulate_test;
