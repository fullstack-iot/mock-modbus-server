use crate::utils::SimulationMode;

pub struct Simulator {
    mode: SimulationMode,
}

pub fn u16s_to_float(nums: [u16; 2]) -> f32 {
    let bits = ((nums[0] as u32) << 16) | (nums[1] as u32);
    f32::from_bits(bits)
}

fn float_to_u16s(num: f32) -> [u16; 2] {
    let bits = num.to_bits();
    let u16_1 = (bits >> 16) as u16;
    let u16_2 = bits as u16;
    [u16_1, u16_2]
}

const STEP: f32 = 1.0;
const START: u8 = 0;
const END: u8 = 100;

impl Simulator {
    pub fn new(mode: SimulationMode) -> Self {
        Self { mode }
    }
    pub fn booleans(&self, slice: &mut [bool]) {
        slice.iter_mut().for_each(|input| *input = rand::random())
    }

    #[allow(dead_code)]
    pub fn u16s(&mut self, slice: &mut [u16; 2]) {
        match self.mode {
            SimulationMode::Random => slice
                .iter_mut()
                .for_each(|input| *input = rand::random::<u16>()),
            SimulationMode::RangeIncrease => {
                let mut float = u16s_to_float(*slice);
                float += STEP;
                if float > END as f32 {
                    float = START as f32;
                }
                *slice = float_to_u16s(float);
            }
        }
    }
}
