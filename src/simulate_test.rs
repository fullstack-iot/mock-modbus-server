use super::*;
use crate::simulate::u16s_to_float;
use crate::utils::SimulationMode;

// this should test the simulator u16s function
// it should convert the u16s to a f32
// it should test the range increase mode by checking that float is going up one by one in the ones place everytime its called
// when it reaches the end of the range it should start over at the beginning
#[test]
fn test_simulator_u16s_increasing() {
    let mut simulator = Simulator::new(SimulationMode::RangeIncrease);
    let mut slice = [0u16; 2];
    let mut float = 0.0;
    for _ in 0..100 {
        simulator.u16s(&mut slice);
        let new_float = u16s_to_float(slice);
        assert!(new_float >= float);
        float = new_float;
    }
    // check for restart
    simulator.u16s(&mut slice);
    let restarted_float = u16s_to_float(slice);
    assert!(restarted_float < float);
}
